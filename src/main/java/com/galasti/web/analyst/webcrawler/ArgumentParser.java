package com.galasti.web.analyst.webcrawler;

public class ArgumentParser {
	
	static final String ARGUMENT_MESSAGE = "Correct application usage site:<url> OPTIONAL: limit:<number>";
	
	String[] arguments;
	
	int limit;
	String url;
	
	public ArgumentParser(String[] args) {
		this.arguments = args;
	}
	
	public int getLimit() {
		return this.limit;
	}
	
	public String getUrl() {
		return this.url;
	}
	
	public boolean parseArguments() {
		if(this.arguments.length == 0) {
			System.err.println(ARGUMENT_MESSAGE);
			return false;
		}

		String urlString = this.arguments[0];
		String[] urlArgs = urlString.split(":", 2);
		
		if(!urlArgs[0].equalsIgnoreCase("url") || urlArgs.length < 2) {
			System.err.println(ARGUMENT_MESSAGE);
			return false;
		}
		this.url = urlArgs[1];
		
		if(this.arguments.length < 2)
			return true;
		
		String limitString = this.arguments[1];
		String limitArgs[] = limitString.split(":");
		if(!limitArgs[0].equalsIgnoreCase("limit") || limitArgs.length < 2) {
			System.err.println(ARGUMENT_MESSAGE);
			return false;
		}
		this.limit = Integer.parseInt(limitArgs[1]);
		
		return true;
	}
}