package com.galasti.web.analyst.webcrawler;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class PageRequester {

	private String url;
	
	public PageRequester(String url) {
		this.url = url;
		
		// Need validation
	}
	
	public String getHTML() {
		URL url;
		HttpURLConnection connection;
		BufferedReader buffer;
		String line;
		String result = "";
		try {
			url = new URL(this.url);
			connection = (HttpURLConnection) url.openConnection();
			connection.setRequestMethod("GET");
			buffer = new BufferedReader(new InputStreamReader(connection.getInputStream()));
			while ((line = buffer.readLine()) != null) {
				result += line;
			}
			buffer.close();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

}
