package com.galasti.web.analyst.webcrawler;

import java.util.HashSet;
import java.util.Set;

public class PageAnalyzer {

	public PageAnalyzer() {
	}
	
	public Set<String> extractEmails(PageRequester requester) {
		
		String html = requester.getHTML();
		return this.getEmails(html);
	}
	
	private Set<String> getEmails(String html) {
		
		Set<String> emails = new HashSet<String>();

		int dotCount = 0;
		int atCount = 0;
		StringBuffer buffer = new StringBuffer();
		for(int i = 0; i < html.length(); i++) {
			final char CHARACTER = html.charAt(i);
			if(CHARACTER == '@')
				++atCount;
			if(CHARACTER == '.')
				++dotCount;

			if( atCount > 1) {
				atCount = dotCount = 0;
				buffer = new StringBuffer();
				continue;
			}

			if(!Character.isAlphabetic(CHARACTER) &&
					CHARACTER != '@' &&
					CHARACTER != '.') 
			{
				if(dotCount > 0 && atCount == 1 && buffer.length() > 5 ) {
					emails.add(buffer.toString());
				}

				atCount = dotCount = 0;
				buffer = new StringBuffer();
				continue;
			}
			buffer.append(CHARACTER);
		}
		
		return emails;
	}
}
