package com.galasti.web.analyst.webcrawler;

import java.util.Set;

public class App 
{
	public static void main( String[] args )
	{
		ArgumentParser parser = new ArgumentParser(args);
		if(!parser.parseArguments())
			return;
		
		Set<String> emails;

		final String URL = parser.getUrl();
		PageAnalyzer analyzer = new PageAnalyzer();
		emails = analyzer.extractEmails(new PageRequester(URL));
		
		for(String email : emails) {
			System.out.println(email);
		}
	}
	
}
